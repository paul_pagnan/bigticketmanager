<?php
    App::import('model','Admin');
    class Event extends AppModel {
        var $name = 'Event';
        public $useTable = 'event';
        
        public function getSettings() {
            $admin = new Admin();
            return $this->find(
                'first',
                array(
                    'conditions' => array(
                        'Event.Id' => $admin->get_setting('current_event')
                    )
                )
            );
        }

        public function updateEvent($data) {
            $admin = new Admin();
            $this->id = $admin->get_setting('current_event');
            $this->set($data);
            
            if ($this->validates()) {
                $this->save($data);
                return true;
            }
            return false;
        }

        public function getErrors() {
            return $this->validationErrors;
        }
    }