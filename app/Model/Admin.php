<?php

class Admin extends AppModel {
    var $name = 'Admin';
    public $useTable = 'settings';

    public function get_setting($type) {
        $setting = $this->find(
            'first',
            array(
                'conditions' => array(
                    'Admin.key' => $type
                )
            )
        );
        if($setting != NULL)
            return $setting['Admin']['value'];
        return false;
    }
}