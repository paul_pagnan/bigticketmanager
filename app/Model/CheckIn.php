<?php

    class CheckIn extends AppModel {
        var $name = 'CheckIn';
        public $useTable = 'check_ins';

        public $hasOne = array(
            'Ticket' => array(
                'className' => 'Ticket',
                'dependent' => false,
                'foreignKey' => 'id',
                'primaryKey' => 'Ticket_ID'
            )
        );

        function ticketCheckIn($ticketId, $over18, $user) {
            if(!empty($ticketId)) {
                return $this->add($ticketId, $over18, $user);
            }
            return false;
        }

        function ticketCheckOut($ticketId) {
            if(!empty($ticketId)) {
                return $this->remove($ticketId);
            }
            return false;
        }

        function getCheckIn($id)
        {
            return $this->read(NULL, $id);
        }

        function isCheckedIn($ticket)
        {
            return $this->hasAny(array("CheckIn.ID" => $ticket['Ticket']['ID']));
        }

        function getAll() {
            return $this->find('all');
        }

        private function add($ticketId, $over18, $user) {
            return $this->save(array(
                'ID' => $ticketId,
                'UserID' => $user,
                'Over18' => $over18
            ));
        }

        private function remove($ticket){
            return $this->delete($this->findCheckInByTicket($ticket)['CheckIn']['ID']);
        }

        private function findCheckInByTicket($ticket) {
            return $this->find('first', array('CheckIn.ID' => $ticket['Ticket']['ID']));
        }
    }
?>