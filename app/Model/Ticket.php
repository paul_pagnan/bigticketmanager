<?php
    App::import('model','Revision');
    class Ticket extends AppModel {
        var $name = 'Ticket';

        public $validate = array(
            'ID' => array(
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Ticket number must not contain characters'
            ),
            'FirstName' => array(
		'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'First Name is required'
            ),
            'LastName' => array(
		'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Last Name is required'
            ),
            'MobileNumber' => array(
                'allowEmpty' => true,
                'rule' => array( 'lengthBetween', 10, 10 ),
                'message' => 'Please enter a valid phone number'
            ),
            'Email' => array(
		'allowEmpty' => true,
                'rule' => 'email',
                'message' => 'Please enter a valid email address'
            ),
            'StudentNumber' => array(
                'Length' => array(
                    'allowEmpty' => true,
                    'rule' => array('minLength', 8),
                    'message' => 'Student number must be 8 digits'
                ),
                'isNumber' => array(
                    'allowEmpty' => true,
                    'rule' => 'numeric',
                    'message' => 'Student number must not contain characters'
                )
            ),
            'AmountPaid' => array(
		'required' => true,
		'allowEmpty' => false,
                'rule' => 'numeric',
                'message' => 'AmountPaid is required'
            )
        );


        function getTicketById ($id) {
            return $this->read(NULL, $id);
        }

        function updateTicket($id, $data) {

            $this->id = $id;
            $this->set($data);
            if ($this->validates()) {
                $revision = new Revision();
                $revision->edited($data);
                $this->save($data);
                return true;
            }
            return false;
        }

        function deleteTicket($id) {
            $revision = new Revision();
            $revision->deleted($this->getTicketById($id));
            return ($this->delete($id) ? true : false);
        }

        function getAll() {
            return $this->find('all');
        }

        function addTicket($data) {
            if ($this->getTicketById($data['Ticket']['ID']) == NULL) {
                $this->set($data);
                if ($this->validates()) {
                    $revision = new Revision();
                    $revision->added($data);
                    $this->save($data);
                    return true;
                }
            }
            return false;
        }

        public function totalSales() {
            $sales = $this->find('all', array(
                'fields' => array(
                    'SUM(Ticket.AmountPaid) AS total'
                )
            ));
            return number_format((int)$sales[0][0]['total'], 2, '.', ',');
        }

        public function getLeaderboard() {
            $data = $this->find('all', array(
                'conditions' => array('Ticket.Seller !=' => ''),
                'fields' => array('Ticket.Seller, COUNT(*) as "count", SUM(Ticket.AmountPaid) as "profit"'),
                'group' => array('Ticket.Seller'),
                'order' => array('SUM(Ticket.AmountPaid) DESC'),
                'limit' => 5,
            ));
            return $data;
        }

        public function getSalesData($timeframe) {
            $condition = "Ticket.Timestamp > NOW() - INTERVAL 7 DAY";
            $format = "%d-%m-%Y";
            switch($timeframe) {
                case 'hourly': $format = "%d-%m-%Y %H:00"; $condition = array('Ticket.Timestamp > NOW() - INTERVAL 1 DAY'); break;
                case 'daily': break;
                case 'weekly': $format = "%u"; $condition = array('Ticket.Timestamp > NOW() - INTERVAL 8 WEEK'); break;
                case 'monthly': $format = "%b-%Y"; $condition = array('Ticket.Timestamp > NOW() - INTERVAL 5 MONTH'); break;
                default: break;
            }

            $data = $this->find('all', array(
                'conditions' => array($condition),
                'fields' => array('DATE_FORMAT(Ticket.Timestamp, "' . $format . '") as "date"', 'COUNT(*) As "total"'),
                'group' => array('date'),
                'order' => array('Ticket.Timestamp ASC'),
                'limit' => 5,
            ));
            return $data;
        }

        function getPrice($big, $uts) {
            $data = $this->find('first', array(
                'conditions' => array(
                    "Ticket.Paid" => 1,
                    "Ticket.BiGMember" => $big,
                    "Ticket.UTSStudent" => $uts
                ),
                'fields' => array("Ticket.AmountPaid")
                )
            );
            return ($data != NULL) ? $data['Ticket']['AmountPaid'] : "";
        }

        public function findTickets($input) {
            return
                $this->find('all',
                    array('conditions' => array('OR' =>
                        array(
                            'Ticket.FirstName LIKE' => "%$input%",
                            'Ticket.LastName LIKE' => "%$input%",
                            'Ticket.Seller LIKE' => "%$input%",
                            'CONCAT(Ticket.FirstName, " ", Ticket.LastName) LIKE' => "%$input%",
                            'Ticket.StudentNumber LIKE' => "%$input%",
                            'Ticket.Email LIKE' => "%$input%",
                        )
                    ))
                );
        }

        public function getErrors() {
            return $this->validationErrors;
        }
    }

?>
