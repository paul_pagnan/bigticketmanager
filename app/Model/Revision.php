<?php
    class Revision extends AppModel {
        var $name = 'Revision';
        public $useTable = 'revisions';

        public function added($ticket) {
            $this->createRevision('add', $ticket);
        }

        public function edited($ticket) {
            $this->createRevision('edit', $ticket);
        }

        public function deleted($ticket) {
            $this->createRevision('delete', $ticket);
        }

        function createRevision($type, $ticket) {
            $user = $this->getCurrentUser();
            $this->save(
                array( "Revision" =>
                    array(
                        'Type' => $type,
                        'TicketID' => $ticket['Ticket']['ID'],
                        'Ticket' => serialize($ticket),
                        'Editor' => $user['username']
                    )
            ));
        }

        function getRevisions($id) {
            return $this->find('all', array(
                    'conditions' => array(
                        'Revision.TicketID' => $id
                    ),
                    'order' => 'Revision.Timestamp DESC'
                ));
        }
    }
