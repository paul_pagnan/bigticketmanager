<?php $this->Html->addCrumb('Tickets', "/tickets"); ?>
<?php $this->Html->addCrumb($ticket['Ticket']['ID'], "/tickets/view/" . $ticket['Ticket']['ID']); ?>
<?php
    if($ticket != NULL)
    { ?>
        <?php echo $this->element('TicketDetails', array('ticket' => $ticket, 'checkedIn' => $checkedIn, 'checkIn' => $checkIn)); ?>
    <?php
    }
    else
    { ?>
           Ticket was not found... <br>
           <a href="/tickets"> Go Back... </a>
    <?php }

?>
