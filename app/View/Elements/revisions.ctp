<div class="table-container">
<?php
    if($paginate) {
        $paginator = $this->Paginator;
?>
            <ul class="pagination right" role="menubar" aria-label="Pagination">
              <li class="arrow <?php echo ($paginator->hasPrev() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasPrev() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasPrev()){ echo $paginator->prev("Prev"); } ?></li>
                <?php
                    echo $this->Paginator->numbers(array(
                     'tag'=>'li',
                     'separator'=>'',
                     'currentTag'=>'a'
                    ));
                ?>
              <li class="arrow <?php echo ($paginator->hasNext() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasNext() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasNext()){ echo $paginator->next("Next"); } ?></li>
            </ul>
    <?php } ?>

    <table class="revisions">
        <thead>

            <?php if($paginate) { ?>
                <tr class="nope">
                    <th><?php echo $paginator->sort('Timestamp', 'Timestamp'); ?></th>
                    <th><?php echo $paginator->sort('TicketNumber', 'Ticket #'); ?></th>
                    <th><?php echo $paginator->sort('Editor', 'Editor'); ?></th>
                    <th><?php echo $paginator->sort('Type', 'Type'); ?></th>
                </tr>
            <?php } else { ?>
                <tr class="nope">
                    <th>Timestamp</th>
                    <th>TicketNumber</th>
                    <th>Editor</th>
                    <th>Type</th>
                </tr>
             <?php } ?>
        </thead>
        <tbody>
            <?php foreach($revisions as $revision) { ?>
            <tr class="odd">
                <td><?php echo $revision['Revision']['Timestamp']; ?></td>
                <td><?php echo $revision['Revision']['TicketID']; ?></td>
                <td><?php echo $revision['Revision']['Editor']; ?></td>
                <td><?php echo $revision['Revision']['Type']; ?></td>
            </tr>
            <?php
                $ticket = unserialize($revision['Revision']['Ticket']);
            ?>
            <tr class="border-bottom">
                <td colspan="5" style="width:100%;">
                    <h4>Revision Details</h4>
                    <div class="table-container">
                        <table>
                            <tr class="nope">
                                <td>
                                    <table>
                                        <tr class="nope">
                                            <th>Ticket No.</th>
                                            <td><?php echo $ticket['Ticket']['ID']; ?></td>
                                        </tr>
                                        <tr class="nope">
                                            <th>Student Number</th>
                                            <td><?php echo $ticket['Ticket']['StudentNumber']; ?></td>
                                        </tr>
                                        <tr class="nope">
                                            <th>First Name</th>
                                            <td><?php echo $ticket['Ticket']['FirstName']; ?></td>
                                        </tr class="nope">
                                        <tr class="nope">
                                            <th>Last Name</th>
                                            <td><?php echo $ticket['Ticket']['LastName']; ?></td>
                                        </tr>
                                        <tr class="nope">
                                            <th>Email</th>
                                            <td><?php echo $ticket['Ticket']['Email']; ?></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr class="nope">
                                            <th>Paid</th>
                                            <td><?php echo ($ticket['Ticket']['Paid'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
                                        </tr>
                                        <tr class="nope">
                                            <th>BiG Member</th>
                                            <td><?php echo ($ticket['Ticket']['BiGMember'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
                                        </tr>
                                        <tr class="nope">
                                            <th>UTS Student</th>
                                            <td><?php echo ($ticket['Ticket']['UTSStudent'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
                                        </tr>
                                        <tr class="nope">
                                            <th>Amount Paid</th>
                                            <td>$<?php echo number_format($ticket['Ticket']['AmountPaid'], 2, '.', ''); ?></td>
                                        </tr>
                                         <tr class="nope">
                                            <th>Seller</th>
                                            <td><?php echo $ticket['Ticket']['Seller']; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <?php if($paginate) { ?>
                            <a href="/tickets/view/<?php echo $revision['Revision']['TicketID'] ?>" class="right button small">View Ticket</a>

                        <?php } ?>

                     </div>
                 </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

    <?php if($paginate) { ?>
        <ul class="pagination right" role="menubar" aria-label="Pagination">
          <li class="arrow <?php echo ($paginator->hasPrev() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasPrev() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasPrev()){ echo $paginator->prev("Prev"); } ?></li>
            <?php
                echo $this->Paginator->numbers(array(
                 'tag'=>'li',
                 'separator'=>'',
                 'currentTag'=>'a'
                ));
            ?>
          <li class="arrow <?php echo ($paginator->hasNext() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasNext() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasNext()){ echo $paginator->next("Next"); } ?></li>
        </ul>
    <?php } ?>

</div>