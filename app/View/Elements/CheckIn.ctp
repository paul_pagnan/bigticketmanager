
  <?php
    if ($checkedIn) { ?>
        <h3>Check In Details:</h3>
        <div class="table-container">
        <table>
            <tr>
                <th>TimeStamp</th>
                <td><?php echo $checkIn['CheckIn']['TimeStamp'] ?></td>
            </tr>
            <tr>
                <th>Checked In By</th>
                <td><?php echo $checkIn['CheckIn']['UserID'] ?></td>
            </tr>
            <tr>
                <th>Over 18</th>
                <td><?php echo ($checkIn['CheckIn']['Over18'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
            </tr>
        </table>
        </div>
    <?php } else { ?>
    <br><br>
    <?php } ?>
  <b>Checked In: </b> <?php echo ($checkedIn ? "<span style='color:green'>YES</span>" : "<span style='color:red'>NO</span>"); ?>
<br><br>
  <?php
      echo $this->Form->create();
      echo (!$checkedIn ? $this->Form->input('CheckIn.Over18', array('type' => 'checkbox', 'label' => 'Over 18?')) : '');
      echo $this->Form->input('CheckedIn', array('type' => 'hidden', 'value' => $checkedIn));
      echo $this->Form->input('TicketId', array('type' => 'hidden', 'value' => $Id));
      echo $this->Form->button((!$checkedIn ? 'Check In' : 'Check Out'), array('type' => 'submit', 'class' => (!$checkedIn ? 'success' : 'alert')));
      echo $this->Form->end();
  ?>