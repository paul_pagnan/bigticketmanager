<ul class="pagination" role="menubar" aria-label="Pagination">
          <li class="arrow <?php echo ($paginator->hasPrev() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasPrev() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasPrev()){ echo $paginator->prev("Prev"); } ?></li>
            <?php
                echo $this->Paginator->numbers(array(
                 'tag'=>'li',
                 'separator'=>'',
                 'currentTag'=>'a'
                ));
            ?>
          <li class="arrow <?php echo ($paginator->hasNext() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasNext() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasNext()){ echo $paginator->next("Next"); } ?></li>
        </ul>