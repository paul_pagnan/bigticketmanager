<?php

    echo $this->Form->create('Ticket', array('type' => 'post'));
    echo $this->Form->input('Ticket.ID', array('type' => 'text', 'label' => 'Ticket Number:'));
    echo $this->Form->input('Ticket.FirstName', array('type' => 'text', 'label' => 'First Name:'));
    echo $this->Form->input('Ticket.LastName', array('type' => 'text', 'label' => 'Last Name:'));
    echo $this->Form->input('Ticket.Email', array('type' => 'email', 'label' => 'Email:'));
    echo $this->Form->input('Ticket.StudentNumber', array('type' => 'text', 'maxLength' => 8, 'label' => 'Student Number:'));
    echo $this->Form->input('Ticket.BiGMember', array('type' => 'checkbox', 'label' => 'BiG/EngSoc Member', 'onclick' => 'getPrice();'));
    echo $this->Form->input('Ticket.UTSStudent', array('type' => 'checkbox', 'label' => 'UTS Student', 'onclick' => 'getPrice();'));
    echo $this->Form->input('Ticket.Paid', array('type' => 'checkbox', 'label' => 'Paid:', 'onclick' => 'getPrice();'));
    if($user != NULL) {
        echo $this->Form->input('Ticket.Seller', array('type' => 'hidden', 'value' => $user));
    }
    else {
        echo $this->Form->input('Ticket.Seller', array('type' => 'text', 'label' => 'Seller:'));
    }
?>
    Amount Paid: <br>
    <div class="row left">
        <div class="large-6 columns">
          <div class="row collapse prefix-radius">
            <div class="small-3 columns">
              <span class="prefix">$</span>
            </div>
            <div class="small-9 columns">
             <?php echo $this->Form->input('Ticket.AmountPaid', array('type' => 'text', 'label' => '', 'default' => '0')); ?>
            </div>
          </div>
         </div>
     </div><br><br><br>

<?php
    echo $this->Form->button('Submit', array('type' => 'submit'));
    echo $this->Form->end();

?>
