<h2>Ticket #<?php echo $ticket['Ticket']['ID']; ?>: <?php echo $ticket['Ticket']['FirstName'] . " " . $ticket['Ticket']['LastName']; ?></h2>
<h5></h5>
<br>

<div class="row">
    <div class="small-12 medium-8 columns ">
        <h3>Ticket Details</h3>
        <div class="table-container">
        <table>
            <tr>
                <th>Ticket No.</th>
                <td><?php echo $ticket['Ticket']['ID']; ?></td>
            </tr>
            <tr>
                <th>Student Number</th>
                <td><?php echo $ticket['Ticket']['StudentNumber']; ?></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><?php echo $ticket['Ticket']['FirstName']; ?></td>
            </tr>
            <tr>
                <th>Last Name</th>
                <td><?php echo $ticket['Ticket']['LastName']; ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $ticket['Ticket']['Email']; ?></td>
            </tr>
            <tr>
                <th>Paid</th>
                <td><?php echo ($ticket['Ticket']['Paid'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
            </tr>
            <tr>
                <th>Member</th>
                <td><?php echo ($ticket['Ticket']['BiGMember'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
            </tr>
            <tr>
                <th>UTS Student</th>
                <td><?php echo ($ticket['Ticket']['UTSStudent'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
            </tr>
            <tr>
                <th>Amount Paid</th>
                <td>$<?php echo number_format($ticket['Ticket']['AmountPaid'], 2, '.', ''); ?></td>
            </tr>
             <tr>
                <th>Seller</th>
                <td><?php echo $ticket['Ticket']['Seller']; ?></td>
            </tr>
        </table>
        </div>
    </div>
    <div class="small-12 medium-4 columns border-left">
        <?php echo $this->element('CheckIn', array('CheckedIn' => $checkedIn, 'Id' => $ticket['Ticket']['ID'], 'checkIn' => $checkIn)); ?>
    </div>
</div>

<br>
<a href="../edit/<?php echo $ticket['Ticket']['ID']; ?>" class="button right">Edit Ticket</a>
<?php
    if($role == 'admin') {
    ?>
    <a href="../delete/<?php echo $ticket['Ticket']['ID']; ?>" onclick="return confirm('Are you sure?')" class="button alert right">Delete Ticket</a>

<?php
} ?>
    <br><hr>
    <h3>Revisions</h3>
    <?php echo $this->element('revisions', array('revisions' => $revisions, 'paginate' => false)); ?>
