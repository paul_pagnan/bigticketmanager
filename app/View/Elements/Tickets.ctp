<div class="table-container">
<?php
    $paginator = $this->Paginator;

    if($tickets){

    ?>

        <ul class="pagination right" role="menubar" aria-label="Pagination">
          <li class="arrow <?php echo ($paginator->hasPrev() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasPrev() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasPrev()){ echo $paginator->prev("Prev"); } ?></li>
            <?php
                echo $this->Paginator->numbers(array(
                 'tag'=>'li',
                 'separator'=>'',
                 'currentTag'=>'a'
                ));
            ?>
          <li class="arrow <?php echo ($paginator->hasNext() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasNext() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasNext()){ echo $paginator->next("Next"); } ?></li>
        </ul>

    <?php
        //creating our table
        echo "<table><thead>";

            // our table header, we can sort the data user the paginator sort() method!
            echo "<tr>";

                // in the sort method, ther first parameter is the same as the column name in our table
                // the second parameter is the header label we want to display in the view
                echo "<th>" . $paginator->sort('ID', 'Tix #') . "</th>";
                echo "<th>" . $paginator->sort('First Name', 'FirstName') . "</th>";
                echo "<th>" . $paginator->sort('Last Name', 'LastName') . "</th>";
                echo "<th>" . $paginator->sort('Student Number', 'StudentNumber') . "</th>";
//                echo "<th>" . $paginator->sort('Email', 'Email') . "</th>";
                echo "<th>" . $paginator->sort('BiG Member', 'Member') . "</th>";
                echo "<th>" . $paginator->sort('UTS Student', 'UTSStudent') . "</th>";
                echo "<th>" . $paginator->sort('Paid', 'Paid') . "</th>";
                echo "<th>" . $paginator->sort('Seller', 'Seller') . "</th>";
//                echo "<th>" . $paginator->sort('Amount Paid', 'AmountPaid') . "</th>";
            echo "</tr></thead><tbody>";

            // loop through the user's records
            foreach( $tickets as $ticket ){
            ?>
                <tr onclick="document.location='/tickets/view/<?php echo $ticket['Ticket']['ID']; ?>'">
             <?php
                    echo "<td><a href='/tickets/view/{$ticket['Ticket']['ID']}'>{$ticket['Ticket']['ID']}</a></td>";
                    echo "<td>{$ticket['Ticket']['FirstName']}</td>";
                    echo "<td>{$ticket['Ticket']['LastName']}</td>";
                    echo "<td>{$ticket['Ticket']['StudentNumber']}</td>";
//                    echo "<td>{$ticket['Ticket']['Email']}</td>";
                    echo "<td>" . ($ticket['Ticket']['BiGMember'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>') . "</td>";
                    echo "<td>" . ($ticket['Ticket']['UTSStudent'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>') . "</td>";
                    echo "<td>" . ($ticket['Ticket']['Paid'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>') . "</td>";
                    echo "<td>{$ticket['Ticket']['Seller']}</td>";
//                    echo "<td>" . number_format($ticket['Ticket']['AmountPaid'], 2, '.', '') . "</td>";
                echo "</tr>";
            }

        echo "</tbody></table>";

        // pagination section

        ?>

        <ul class="pagination right" role="menubar" aria-label="Pagination">
          <li class="arrow <?php echo ($paginator->hasPrev() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasPrev() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasPrev()){ echo $paginator->prev("Prev"); } ?></li>
            <?php
                echo $this->Paginator->numbers(array(
                 'tag'=>'li',
                 'separator'=>'',
                 'currentTag'=>'a'
                ));
            ?>
          <li class="arrow <?php echo ($paginator->hasNext() ? "" : "unavaliable"); ?>" <?php echo (!$paginator->hasNext() ? "aria-disabled='true'" : ""); ?>><?php if($paginator->hasNext()){ echo $paginator->next("Next"); } ?></li>
        </ul>
<?php
    }

    // tell the user there's no records found
    else{
        echo "No Tickets found.";
    }
?>
</div>