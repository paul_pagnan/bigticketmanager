<!--
    <?php
        if($event != NULL) {
    ?>
        <div class="row">
            <a href="<?php echo $event['Event']['Facebook']; ?>" title="Open Facebook event" target="_blank" ><img class="header" src="<?php echo $event['Event']['Image']; ?>" /> </a> <br><br>
            <h3>Event Countdown</h3>
            <?php
                $date = $event['Event']['Date'];
                $remaining = $date - $time;
                $days_remaining = floor($remaining / 86400);
                $hours_remaining = floor(($remaining % 86400) / 3600);
                $minutes_remaining = floor(($remaining % 86400) / (3600 * 2));
            ?>
        </div>

    <?php } ?>
-->

    <div class="row">
        <div class="large-6 medium-8 small-12 small-centered columns">
            <h3>Find Ticket <span><a href="#" data-dropdown="hover1" data-options="is_hover:true; hover_timeout:5000"><i class="fa fa-question-circle"></i></a></span></h3>

            <ul id="hover1" class="f-dropdown" data-dropdown-content>
                <li><a>Use the search box to find tickets by ticket number, name, student number etc. <br><br> Entering the seller name will display all tickets sold by that person. <br><br> Entering the ticket number will take you straight to that ticket</a></li>
            </ul>

            <div class="row text-center">
                <?php echo $this->Form->create(); ?>
                <div class="small-8 medium-9 padding-right columns">
                    <?php echo $this->Form->input('Search', array('type' => 'text', 'label' => '', 'class' => 'btn-height', 'placeholder' => "Enter TicketNumber, Name, Seller, Student Number etc." )); ?>
                </div>
                <div class="small-4 medium-3 padding-left columns">
                    <?php echo $this->Form->button('Search', array('type' => 'submit', 'class ' => "button small")); ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="text-center">
        <?php echo $this->Html->link('New Ticket', array( 'controller' => 'tickets', 'action' => 'add'), array('class' => 'button success small')); ?>
        <br>
        <b>Your assigned tickets are: </b><?php echo $ticketAssignment; ?> <br><br>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <a href="<?php echo $this->Html->url(array('controller' => 'tickets', 'action' => 'index')); ?>">
                    <div class="small-12 medium-6 columns box color-primary">
                        <h3 class="white"><u>Tickets &nbsp;&raquo;</u></h3>
                        <b>Total Tickets: </b> <?php echo $tickets; ?> <br>
                        <b>Net Sales: </b> $<?php echo $sales; ?><br>
                        <b>Paid: </b> <?php echo $paid; ?> /
                        <b>Non-Paid: </b> <?php echo $nonPaid; ?>
                        <br><br>
                    </div>
                </a>
                <a href="<?php echo $this->Html->url(array('controller' => 'checkIns', 'action' => 'index')); ?>">
                    <div class="small-12 medium-6 columns box color-secondary">
                        <h3 class="white"><u>Check Ins &nbsp;&raquo;</u></h3>
                        <b>Check Ins: </b> <?php echo $checkins; ?> <br>
                        <b>Non-Check Ins: </b> <?php echo $tickets - $checkins; ?>
                        <br><br><br>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="row" data-equalizer>
                <div class="small-12 medium-6 columns box color-tertiary" data-equalizer-watch>
                    <h3 class="white">Sales</h3>
                    <div class="sales-btn-container text-center">
                        <a class="sales-btn" val='hourly'>Hourly</a>
                        <a class="sales-btn active" val='daily'>Daily</a>
                        <a class="sales-btn" val='weekly'>Weekly</a>
                        <a class="sales-btn" val='monthly'>Monthly</a>
                    </div>
                    <div class="row" >
                        <div class="small-12 medium-10 large-10 columns small-centered">
                            <div class="sales">
                                <canvas id="salesGraph" width=400 height=250></canvas>
                            </div>
                        </div>
                     </div>
                </div>

                <div class="small-12 medium-6 columns box color-quart" data-equalizer-watch>
                    <h3 class="white">Leaderboard</h3>
                    <table class="minimal small-12">
                        <thead>
                            <th class="small-1 text-center"></th>
                            <th class="small-5">Seller</th>
                            <th class="small-3 text-center">Tickets Sold</th>
                            <th class="small-4 text-center">Sales</th>
                        </thead>
                            <?php 
                                $count = 1;
                                foreach($leaderboard as $seller) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $count; ?></td>
                                    <td><?php echo $seller['Ticket']['Seller']; ?></td>
                                    <td class="text-center"><?php echo $seller[0]['count']; ?></td>
                                    <td class="text-center">$<?php echo number_format($seller[0]['profit'], 2, ".", ","); ?></td>
                                </tr>
                            <?php 
                               $count++;
                                } 
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="small-12 text-center">
            <a href="/revisions" class="button small">View All Revisions</a>
        </div>
    </div>
    <?php if($role == 'admin') { ?>
         <h3>Admin</h3>
         <hr class="no-margin">
         <br>
         <?php echo $this->Html->link('Add User', array( 'controller' => 'users', 'action' => 'add')); ?> <br>
         <?php echo $this->Html->link('Edit Ticket Assignments', array( 'controller' => 'admin', 'action' => 'ticketAssignments')); ?> <br>
    <?php } ?>





