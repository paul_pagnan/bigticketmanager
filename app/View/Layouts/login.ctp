<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'BiG Ticket Manager -');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<meta name="viewport" content="width=device-width" />
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('app');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
<br>
<br>
    <div class="row">
        <div class="large-8 small-centered columns white">
        <div class="text-center">
            <img class="logo" src="/img/Logo_Trans.png">
        </div>
        <br>
            <div id="container" class="text-left">
                <div id="content">
                    <h1><?php echo $this->fetch('title'); ?></h1>
                    <hr class='no-margin'>
                    <br>
                   <div class="row">
                       <div class="small-12 small-centered columns">
                            <?php echo $this->Session->flash(); ?>
                            <?php echo $this->fetch('content'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

	<?php echo $this->element('sql_dump'); ?>
	<?php echo $this->Html->script('modernizr'); ?>
	<?php echo $this->Html->script('jquery.min'); ?>
	<?php echo $this->Html->script('foundation.min'); ?>
	<?php echo $this->Html->script('foundation/foundation.alert'); ?>
	<?php echo $this->Html->script('app'); ?>
</body>
</html>
