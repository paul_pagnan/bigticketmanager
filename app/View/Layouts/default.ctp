<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'BiG Ticket Manager -');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
    <meta name="viewport" content="width=device-width" />
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('app');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
    <div class="row">
        <div class="large-12">

        <span class="grey right">Hi, <?php echo $username; ?> &nbsp; | &nbsp; <a href="/users/logout" class='white'>Logout</a></span> <br>
        <div class="text-center">
            <a href="/"><img class="logo" src="/img/Logo_Trans.png"></a>
        </div>
        <br>
            <div id="container" class="text-left">
                <div id="content">
                    <?php echo $this->Html->getCrumbs(' > ', 'Home'); ?>
                    <h1><?php echo $this->fetch('title'); ?></h1>
                    <hr class='no-margin'>
                    <br>
                    <?php echo $this->Session->flash(); ?>

                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>
	</div>

	<?php echo $this->element('sql_dump'); ?>
	<?php echo $this->Html->script('modernizr'); ?>
    <?php echo $this->Html->script('jquery.min'); ?>
    <?php echo $this->Html->script('foundation.min'); ?>
    <?php echo $this->Html->script('foundation/foundation.alert'); ?>
    <?php echo $this->Html->script('foundation/foundation.equalizer'); ?>
    <?php echo $this->Html->script('jExpand'); ?>
    <?php echo $this->Html->script('jquery.datetimepicker'); ?>

    <?php echo $this->Html->script('app'); ?>
    <?php
        if($this->request->here == '/' || $this->request->here == '/pages') {
            echo $this->Html->script('chart.min');
            echo $this->Html->script('chart.sales');
        }
    ?>

</body>
</html>
