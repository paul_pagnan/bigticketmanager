<?php $this->Html->addCrumb('Users', "/users"); ?>
<?php $this->Html->addCrumb("Add", "/users/add"); ?>

<!-- app/View/Users/add.ctp -->
<div class="row">
<div class="small-12 large-8 small-centered columns"
<div class="users form">

<?php echo $this->Form->create('User');?>
        <?php echo $this->Form->input('username');
        echo $this->Form->input('email');
        echo $this->Form->input('password');
        echo $this->Form->input('password_confirm', array('label' => 'Confirm Password *', 'maxLength' => 255, 'title' => 'Confirm password', 'type'=>'password'));
        echo $this->Form->input('role', array(
            'options' => array( 'user' => 'user', 'admin' => 'admin')

        ));
?>
<?php
    $options = array(
         'label' => 'Add User',
          'class' => 'button right',
          'title' => 'Click here to add the user'
     );
     echo $this->Form->end($options);
?>
</div>
</div>
</div>
