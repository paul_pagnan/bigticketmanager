<?php $this->Html->addCrumb('Checkins', "/checkIns"); ?>
<a class="button small warning right" href="/checkIns/not">View Non Checkins</a>
<br><br><br>
<div class="table-container">
<table>
    <thead>
        <tr>
            <th>TimeStamp</th>
            <th>Ticket #</th>
            <th>Full Name</th>
            <th>Student Number</th>
            <th>Over 18</th>
            <th>Checked In By</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($CheckIns as $CheckIn): ?>
        <tr onclick="document.location= '/tickets/view/<?php echo $CheckIn['Ticket']['ID']; ?>'">
            <td><?php echo $CheckIn['CheckIn']['TimeStamp']; ?></td>
            <td><a href="/tickets/view/<?php echo $CheckIn['Ticket']['ID']; ?>"><?php echo $CheckIn['Ticket']['ID']; ?></a></td>
            <td><?php echo $CheckIn['Ticket']['FirstName'] . " " . $CheckIn['Ticket']['LastName']; ?></td>
            <td><?php echo $CheckIn['Ticket']['StudentNumber']; ?></td>
            <td><?php echo ($CheckIn['CheckIn']['Over18'] ? '<span style="color:green">YES</span>' : '<span style="color:red">NO</span>'); ?></td>
            <td><?php echo $CheckIn['CheckIn']['UserID']; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>