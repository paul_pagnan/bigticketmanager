<?php

echo $this->Form->create('User', array('type' => 'post'));

//var_dump($users);

?>
<h3>Assign Tickets</h3>
<b>Auto Distribute:</b>
<div class="row">
    <div class="medium-3 columns">
        <?php echo $this->Form->input('numTickets', array('type' => 'text', 'label' => 'Number of Tickets:', 'value' => '0')); ?>
    </div>
    <div class="medium-9 columns">
        <br>
        <a class="button tiny" onclick="distribute()" >Distribute</a>
    </div>
</div>

<table>
    <thead>
        <tr>
            <th>Username:</th>
            <th>Ticket Assignments:</th>
        </tr>
    </thead>
    <tbody>
<?php
foreach($users as $user) {
?>
        <tr>
            <td>
                <b><?php echo $user['User']['username']; ?></b>
            </td>
            <td>
                <?php echo $this->Form->input($user['User']['id'], array('type' => 'text', 'label' => '', 'class' => 'user', 'value' => $user['User']['ticketAssignments'])); ?>
            </td>
        </tr>

<?php } ?> 
</tbody>
</table>

<div class="row">
    <div class="small-12 columns text-right">
        <?php
        echo $this->Form->button('Submit', array('type' => 'submit'));
        ?>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<script>
    function distribute() {
        var numTickets = $("#UserNumTickets").val();
        var users = $(".user");
        console.log(users.length);
        var numUsers = users.length;
        
        if(numTickets > numUsers) {
            var amount  = parseInt(numTickets / numUsers);
            for(var i = 0; i < numUsers; i++) {
                var current = $(".user")[i];
                var strBuild = "#% - #%";
                
                var start = (amount * i) + 1;
                var end = (i == (numUsers - 1)) ? numTickets : (start + amount);
                        
                strBuild = strBuild.replace("%", start).replace("%", end);
                $(current).val(strBuild);
            }
        }
        else
            alert("Not enough tickets to evenly distribute");
    }
</script>