<?php

    echo $this->Form->create('Event', array('type' => 'post'));
    echo $this->Form->input('Event.Title', array('type' => 'text', 'label' => 'Title:'));
    echo $this->Form->input('Event.Image', array('type' => 'text', 'label' => 'Event Image URL:'));
    echo $this->Form->input('Event.Facebook', array('type' => 'text', 'label' => 'Facebook Event URL:'));
    echo $this->Form->input('Event.Date', array('type' => 'text', 'size' => '20', 'label' => 'Date/Time:', 'value' => date('j/m/Y h:i'), 'id' => 'dp1', 'data-date-format' => 'dd/mm/yyyy hh:ii'));
    echo $this->Form->button('Submit', array('type' => 'submit'));
    echo $this->Form->end();
?>
