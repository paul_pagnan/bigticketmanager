$(document).ready(function() {
    genGraph('daily');
});

$(".sales-btn").click( function(event) {
    $(".active").removeClass("active");
    $(event.target).addClass("active");
    genGraph($(event.target).attr('val'));
});

function genGraph(request) {
    $.getJSON( "/Tickets/salesGraph/" + request, function( data ) {
        $("#salesGraph").fadeOut('slow');
        $("#salesGraph").remove();
        $(".sales").append('<canvas id="salesGraph" width=400 height=250></canvas>');
        var keys = [];
        var vals = [];
        $.each( data, function( key, val ) {
            keys.push( val[0].date );
            vals.push( val[0].total );
        });
        var data = {
            labels: keys,
            datasets: [
                {
                    label: "Sales",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: vals
                }
            ]
        };
        
        options = {
            responsive: true, 
            scaleFontColor: "#c6c6c6"
        };
        
        var ctx = document.getElementById("salesGraph").getContext("2d");
        var myLineChart = new Chart(ctx).Line(data, options);
    });   
    
}