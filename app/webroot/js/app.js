// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function() {
    $('.revisions').jExpand();
    $('#dp1').datetimepicker();
    $(".fade").delay(2000).fadeOut(1000);
});

function getPrice() {
    if($("#TicketPaid").is(':checked')) {
        var request = ($("#TicketBiGMember").is(':checked') ? "1" : "0") + "/" + ($("#TicketUTSStudent").is(':checked') ? "1" : "0");

        $.getJSON("/Tickets/getPrice/" + request, function (data) {
            
		$("#TicketAmountPaid").val((data != "") ? data : "0");
        });
    }
    else
    {
        $("#TicketAmountPaid").val("0");
    }
}

