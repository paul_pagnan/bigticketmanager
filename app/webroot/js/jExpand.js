(function($){
    $.fn.jExpand = function(){
        var element = this;

        //$(element).find("tr:odd:not(.nope)").addClass("odd");
        $(element).find("tr:not(.odd, .nope)").hide();
        //$(element).find("tr:first-child").show();

        $(element).find("tr.odd").click(function() {
            $(this).next("tr").toggle('fast');
            $(this).toggleClass('bold');
        });
        
    }
})(jQuery); 