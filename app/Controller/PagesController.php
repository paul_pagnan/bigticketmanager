<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
        $this->loadModel('Ticket');
        $this->loadModel('CheckIn');
        $this->loadModel('Event');
        $this->loadModel('User');

        
        if(!empty($this->request->data)) {
            $input = strip_tags($this->request->data['Ticket']['Search']);

            if(is_numeric($input) && strlen($input) < 8) {
                $ticketFound = ($this->Ticket->getTicketById($input) != NULL);
                if ($ticketFound) {
                    $this->redirect(array(
                        "controller" => "Tickets",
                        "action" => "view",
                        $input
                    ));
                }
            } else {
                $this->redirect(array(
                    "controller" => "Tickets",
                    "action" => "search",
                    $input
                ));
            }
            $this->Session->setFlash(__("Ticket number was not found"), 'flash_notification');
        }

        $event = $this->Event->getSettings();

        $this->set('event', $event);

        $this->set('checkins', $this->CheckIn->find('count'));
        $this->set('tickets', $this->Ticket->find('count'));

        $this->set('paid', $this->Ticket->find('count', array('conditions' => array('Ticket.Paid' => "1"))));
        $this->set('nonPaid', $this->Ticket->find('count', array('conditions' => array('Ticket.Paid' => "0"))));

        $this->set('sales', $this->Ticket->totalSales());
        $this->set('ticketAssignment', $this->User->getAssignment($this->Auth->user('id')));
        
        $this->set('salesData', $this->Ticket->getSalesData('daily'));
        $this->set('leaderboard', $this->Ticket->getLeaderboard());

        $this->set('role', $this->Auth->user('role'));
        $this->set('title_for_layout', ($event != NULL ? $event['Event']['Title'] : 'Home'));
	}
}
