<?php
class AdminController extends AppController
{
    var $name = 'Admin';
    
    function ticketAssignments() {
        if($this->Auth->user('role') == 'admin') { 
            $data = $this->request->data;
            $this->loadModel('User');
            
            if (empty($data)) {
                $data = $this->User->getAssignments();
            }
            
            else {
                if ($this->User->updateAssignments($data)) {
                    $this->Session->setFlash(__("Assignments updated successfully"), 'flash_notification-green');
                    $this->redirect(array('controller' => 'pages', 'action' => 'index'));
                }
                else {
                    $this->displayErrors($this->Admin->getErrors());
                }
            }
            $this->set('users', $data);
            $this->set('title_for_layout', 'Ticket Assignments');
        }
    }
}