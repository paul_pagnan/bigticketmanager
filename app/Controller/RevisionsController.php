<?php
    class RevisionsController extends AppController
    {
        var $name = 'Revisions';
        public $components = array('Paginator');
        public $paginate = array(
            'limit' => 30,
            'order' => array(
                'Timestamp' => 'desc'
            )
        );

        function index() {
            $this->Paginator->settings = $this->paginate;
            $data = $this->Paginator->paginate('Revision');
            $this->set('revisions', $data);
        }

    }