<?php
    class TicketsController extends AppController {
        
        var $name = 'Tickets';
        public $components = array('Paginator');
        public $paginate = array(
            'limit' => 30,
            'order' => array(
                'ID' => 'asc'
            )
        );

        function index() {
            $this->Paginator->settings = $this->paginate;
            $data = $this->Paginator->paginate('Ticket');
            $this->set('count', $this->Ticket->find('count'));

            $this->set('sales', $this->Ticket->totalSales());
            $this->set('tickets', $data);
            $this->set('title_for_layout', 'All Tickets');
        }

        function salesGraph($val = 'daily') {
            $this->autoRender = false;
            $data = $this->Ticket->getSalesData($val);
            $this->set('data', $data);
            $this->set('_serialize', 'data');
            echo json_encode($data);
        }

        function getPrice($big, $uts) {
            $this->autoRender = false;
            $data = $this->Ticket->getPrice($big, $uts);
            $this->set('data', $data);
            $this->set('_serialize', 'data');
            echo json_encode($data);
        }
        
        function view($id = NULL) {
            $this->loadModel('CheckIn');
            $this->loadModel('Revision');
            $user = $this->Auth->user('username');

            if (!empty($this->request->data)) {
                if ($this->_checkIn($this->request->data, $user)) {
                    $this->Session->setFlash(__("Operation performed successfully"), 'flash_notification-green');
                } else {
                    $this->Session->setFlash(__("Oh no, something went wrong!"), 'flash_notification');
                }
            }
            $data = $this->Ticket->getTicketById($id);
            $this->set('ticket', $data);
            $this->set('role', $this->Auth->user('role'));

            $this->set('revisions', $this->Revision->getRevisions($id));

            $this->set('checkedIn', $this->CheckIn->isCheckedIn($data));
            $this->set('checkIn', $this->CheckIn->getCheckIn($id));
            $this->set('title_for_layout', ($data != NULL ? 'Ticket #' . $id : 'Ticket Not Found'));
        }

        function add() {
            $data = $this->request->data;

            if (!empty($data)) {
                if ($this->Ticket->addTicket($data)) {
                    $this->Session->setFlash(__("Ticket added successfully"), 'flash_notification-green');
                    $this->redirect(array('action' => 'index'));
                }
                else {
                    $this->displayErrors($this->Ticket->getErrors());
                    if ($this->Ticket->getTicketById($data['Ticket']['ID']) != NULL) {
                        $this->Session->setFlash(__("Ticket already exists"), 'flash_notification');
                    }
                }
            }
            $this->set('user', $this->Auth->user('username'));
            $this->set('title_for_layout', 'Add Ticket');
        }

        function edit($id) {
            $data = $this->request->data;
            if (empty($data)) {
                $this->data = $this->Ticket->getTicketById($id);
            }

            if (!empty($data)) {
                if($this->Ticket->getTicketbyID($data['Ticket']['ID']) != NULL && $data['Ticket']['ID'] != $id ) {
                    $this->Session->setFlash(__("Ticket number already exists"), 'flash_notification');

                } else {
                          if ($this->Ticket->updateTicket($id, $data)) {
                              $this->Session->setFlash(__("Ticket updated successfully"), 'flash_notification-green');
                              $this->redirect('/tickets/view/' . $id);
                          } else {
                              $this->displayErrors($this->Ticket->getErrors());
                          }
                }
            }
            $this->set('user', NULL);
            $this->set('title_for_layout', 'Edit Ticket');
        }

        function delete($id) {
            if($this->Auth->user('role') == 'admin') {
                if ($this->Ticket->deleteTicket($id)) {
                    $this->Session->setFlash(__("Ticket deleted successfully"), 'flash_notification-green');
                    $this->redirect('/tickets');
                } else {
                    $this->Session->setFlash(__("Something went wrong!"), 'flash_notification');
                }
                $this->set('title_for_layout', 'Delete Ticket');
            }
        }

        function _checkIn($data, $user) {
            $this->loadModel('CheckIn');

            if($data['Ticket']['CheckedIn'] == "1") {
                return $this->CheckIn->ticketCheckOut((int)$data['Ticket']['TicketId']);
            }
            else {
                return $this->CheckIn->ticketCheckIn((int)$data['Ticket']['TicketId'], (bool)$data['CheckIn']['Over18'], $user);
            }
        }


        function search($input = NULL) {
            $input = strip_tags($input);
            $paginate = array(
                'limit' => 30,
                'conditions' => array('OR' =>
                    array(
                        'Ticket.FirstName LIKE' => "%$input%",
                        'Ticket.LastName LIKE' => "%$input%",
                        'Ticket.Seller LIKE' => "%$input%",
                        'CONCAT(Ticket.FirstName, " ", Ticket.LastName) LIKE' => "%$input%",
                        'Ticket.StudentNumber LIKE' => "%$input%",
                        'Ticket.Email LIKE' => "%$input%",
                    )
                ),
                'order' => array(
                    'Ticket.ID' => 'asc'
                )
            );
            $this->Paginator->settings = $paginate;
            $tickets = $this->Paginator->paginate('Ticket');

            if($tickets == NULL) {
                $this->Session->setFlash(__("No results found."), 'flash_notification');
            }

            $this->set('tickets', $tickets);
            $this->set('title_for_layout', "Results for: '$input'");
        }

    }
?>
