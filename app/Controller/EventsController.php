<?php
    class EventsController extends AppController
    {
        var $name = 'Events';

        function update() {
            $data = $this->request->data;

            if (empty($data)) {
                $this->data = $this->Event->getSettings();
            }
            else {
                if ($this->Event->updateEvent($data)) {
                    $this->Session->setFlash(__("Event updated successfully"), 'flash_notification-green');
                    $this->redirect(array('controller' => 'pages', 'action' => 'index'));
                }
                else {
                    $this->displayErrors($this->Event->getErrors());
                }
            }
//            $this->set('data', $data);
            $this->set('title_for_layout', 'Event Settings');
        }
    }