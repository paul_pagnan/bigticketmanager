<?php
    class CheckInsController extends AppController
    {
        var $name = 'CheckIns';
        public $components = array('Paginator');


        function index()
        {
            $this->set('CheckIns', $this->CheckIn->getAll());
            $this->set('title_for_layout', 'All CheckIns');
        }

        function not()
        {
            $this->loadModel('Ticket');
            $checkIns = $this->CheckIn->find('all', array('fields' => 'CheckIn.ID'));
            $notArr = array();
            foreach($checkIns as $checkIn) {
                array_push($notArr, (int)$checkIn['CheckIn']['ID']);
            }

            $paginate = array(
                'limit' => 30,
                'conditions' => array('NOT' => array('Ticket.ID' => $notArr)),
                'order' => array(
                    'Ticket.ID' => 'asc'
                )
            );
            $this->Paginator->settings = $paginate;
            $foundTickets = $this->Paginator->paginate('Ticket');

            $this->set('tickets', $foundTickets);
            $this->set('title_for_layout', 'Not CheckIns');
        }
    }
?>