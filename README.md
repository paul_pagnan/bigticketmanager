# BiG Ticket Manager

BiG ticket manager provides an easy way to manage paper ticket sales for events. The system allows individual users to be set up, each with the ability to add tickets to the database. At the time of the event, the system allows each ticket to be checked in at the door, allowing for attendance tracking on the day.

## Features
* Responsive front end design to allow easy mobile use
* Multi user access
* Add and edit multiple tickets 
* Check in tickets at the door for attendance tracking
* Search functionality, allowing tickets to be easily found
* Sales dashboard
* Leader boards
* Revision history to track user activity


## Future Scope
I plan to extend this applications functionality to include online ticket sales as well. Allowing the combination of paper sales and online sales into the one system. This would require setting up a payment gateway along with a publicly available page in order for tickets to be purchased. 
I also plan on creating an Android app which allows tickets to be more easily checked at the door. Users will simply be able to scan the ticket using their smartphone to check in.


## Screenshots
### Login
![screencapture-tickets-utsbig-com-au-users-login-1428459990998.png](https://bitbucket.org/repo/6jB6Lq/images/211310991-screencapture-tickets-utsbig-com-au-users-login-1428459990998.png)

### Dashboard
![screencapture-tickets-utsbig-com-au-pages-1428459844528.png](https://bitbucket.org/repo/6jB6Lq/images/3018022247-screencapture-tickets-utsbig-com-au-pages-1428459844528.png)

### Ticket
![screencapture-tickets-utsbig-com-au-tickets-view-2-1428459944179.png](https://bitbucket.org/repo/6jB6Lq/images/1463837342-screencapture-tickets-utsbig-com-au-tickets-view-2-1428459944179.png)

### Revision History
![screencapture-tickets-utsbig-com-au-revisions-1428459968129.png](https://bitbucket.org/repo/6jB6Lq/images/1714569856-screencapture-tickets-utsbig-com-au-revisions-1428459968129.png)